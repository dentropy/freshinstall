# Disable SSH Passwords

* [Tutorial](https://www.cyberciti.biz/faq/how-to-disable-ssh-password-login-on-linux/)

``` bash
sudo apt install openssh-server
sudo systemctl start ssh
sudo systemctl enable ssh
sudo nano /etc/ssh/sshd_config
# Search and change the following lines
# ChallengeResponseAuthentication no
# PasswordAuthentication no
# UsePAM no
# PermitRootLogin no
sudo systemctl restart ssh
```