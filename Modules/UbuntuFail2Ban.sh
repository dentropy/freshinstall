#!/bin/bash

# For the people with energy
# https://www.techrepublic.com/article/how-to-install-fail2ban-on-ubuntu-server-18-04/

sudo su
sudo apt-get -y update
sudo apt-get -y upgrade
sudo systemctl start fail2ban
sudo systemctl enable fail2ban