#!/bin/bash


#Install from default repositories
apt -y install python3 python3-pip python3-dev
apt -y install curl
apt -y install git
apt -y install vim
apt -y install tmux
apt -y install x11vnc
apt -y install nmap
apt -y install openvpn
apt -y install screen
apt -y install net-tools
apt -y install youtube-dl
apt -y install htop
apt -y install bsdgames
apt -y install resolvconf
apt -y install lynx

# Install and configure thefuck
pip3 install thefuck
echo "eval $(thefuck --alias)" >> ~/.bashrc


# Install node version manager
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
echo 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")' >> ~/.bashrc
echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm"'  >> ~/.bashrc
source ~/.bashrc

#Actually install node
nvm install node
nvm install-latest-npm
nvm use node

# Funny stuff
apt-get -y install cmatrix
apt-get -y install sl

# Other scripts
wget get.docker.com -O docker.sh
bash docker.sh
rm docker.sh
usermod -aG docker $USER
apt install -y docker-compose
